const fs = require('fs');

module.exports = (app) => {
  let config = app.config;
  let databaseConfig = config.database;
  let defaultStore = databaseConfig.models.defaultStore;

  if (databaseConfig.stores[defaultStore].initialize) {

    let query = require('./Phrase.json');

    return app.orm.Phrase.bulkCreate(query,{ignoreDuplicates:true}).then(() => {
      app.log.info('Phrase sql data insertion successful')
    }).catch((e) => {
      app.log.error(e);
    });

  }
}

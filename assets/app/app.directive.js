(function(angular) {

  'use strict';
  /**
   * @ngdoc overview
   * @name module
   * @description the module contains the directives
   */
  angular.module('app.directive', []);

})(angular);
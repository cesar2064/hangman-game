(function (module) {
  'use strict';

  module.controller('ConquerorCtrl', [    
    'playerFactory',
    '$location',
    controller
  ]);

  function controller(playerFactory, $location) {

    var ctrl = this;
    
    ctrl.conqueror = playerFactory.data;

    /**
     * @ngdoc function
     * @name playAgain
     * @description this method locates the application in the root view      
     */
    ctrl.playAgain = function(){
      $location.path('/');
    };

  };
})(angular.module('app.controller'));
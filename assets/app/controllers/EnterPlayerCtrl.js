(function (module) {
  'use strict';

  module.controller('EnterPlayerCtrl', [
    'playerFactory',
    'playerService',
    'CONSTANTS',
    '$location',
    controller
  ]);

  function controller(playerFactory, playerService, CONSTANTS, $location) {

    var ctrl = this;

    ctrl.player = playerFactory.data;

    //this variable is setted in the view (angular forms)
    ctrl.playerForm = null;

    //getting player results
    playerService.getAll().then(function (results) {
      ctrl.playerResults = results.data;
    });


    ctrl.validatePlayerName = function () {
      var playerName = ctrl.playerForm.playerName
      return playerName.$dirty && playerName.$error.required !=undefined
    };

    ctrl.start = function () {

      if (ctrl.playerForm.$valid) {
        //a flag that allows the user goes to the rounds page        
        playerFactory.ready = true;
        $location.path('/rounds');
      } else {
        ctrl.playerForm.playerName.$dirty = true;
      }
    }

  };
})(angular.module('app.controller'));

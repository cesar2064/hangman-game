(function(module) {
    'use strict';

    module.controller('RoundCtrl', [
        'playerService',
        'phraseService',
        'playerFactory',
        'buttonFactory',
        'CONSTANTS',
        '$location',
        'firstPhrase',
        controller
    ]);

    function controller(playerService, phraseService, playerFactory, buttonFactory, CONSTANTS, $location, firstPhrase) {

        var ctrl = this;

        ctrl.matchedLetters = 0;

        ctrl.usedPhrasesIndexes = [firstPhrase.id]

        ctrl.player = playerFactory.data;
        
        ctrl.matchLetter = function(letter) {
            var letter = letter.toUpperCase();
            var splittedPhrase = ctrl.splittedPhrase;
            var letterMatched = false;

            for (var i = splittedPhrase.length; i--;) {

                var current = splittedPhrase[i];

                if (current.value === letter) {
                    ctrl.matchedLetters++;
                    letterMatched = true;
                    current.show = true;
                }
            }

            return letterMatched;
        };

        ctrl.splitPhrase = function(phrase) {
            return phrase.toUpperCase().split('').map(function(letter) {
                if (letter === ' ') {
                    ctrl.matchedLetters++;
                    return { value: letter, show: true }
                }
                return { value: letter, show: false }
            });
        };


        ctrl.nextButton = false;

        ctrl.roundIndex = 1;

        ctrl.splittedPhrase = ctrl.splitPhrase(firstPhrase.text);

        ctrl.movesLeft = CONSTANTS.MOVE.MAX;

        ctrl.buttons = buttonFactory.data;

        ctrl.next = function() {
            phraseService.getRandomIdNotIn(ctrl.usedPhrasesIndexes).then(function(phrase) {
                ctrl.usedPhrasesIndexes.push(phrase.id);
                ctrl.roundIndex++;
                ctrl.nextButton = false;
                ctrl.movesLeft = CONSTANTS.MOVE.MAX;
                buttonFactory.reset();
                ctrl.matchedLetters = 0;
                ctrl.splittedPhrase = ctrl.splitPhrase(phrase.text);
            })
        }

        ctrl.playerInput = function(button) {

            if (ctrl.matchLetter(button.letter)) {
                button.disabled = true;
            } else {
                ctrl.movesLeft--;
            }

            if (ctrl.movesLeft == 0) {
                buttonFactory.disableAll();
                ctrl.player.lost = 1;
                $location.path('/conqueror');
            }

            if (ctrl.matchedLetters === ctrl.splittedPhrase.length) {
                buttonFactory.disableAll();
                if (ctrl.roundIndex === CONSTANTS.ROUNDS.MAX) {
                    ctrl.player.won = 1;
                    $location.path('/conqueror');
                }
                else {
                    ctrl.nextButton = true;
                }
            }

        };


    };
})(angular.module('app.controller'));

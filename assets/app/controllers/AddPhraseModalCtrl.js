(function (module) {

  'use strict'
  module.controller('AddPhraseModalCtrl', [
    '$uibModalInstance',
    'phraseService',
    controller
  ]);

  function controller($uibModalInstance,phraseService) {

    var ctrl = this;

    ctrl.phraseForm;

    ctrl.phrase = {
      text:''
    }

    ctrl.validatePhrase = function () {
      var phrase = ctrl.phraseForm.phrase
      return phrase.$dirty && phrase.$error.required
    };

    ctrl.save = function () {
      var phrase = ctrl.phrase;

      phrase.text = phrase.text.trim();

      phraseService.save(phrase).then(function(result){
        $uibModalInstance.close('cancel');
      }).catch(function(){
        alert('The Phrase already exists')
      })
      
    };

    ctrl.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
})(angular.module('app.controller'));
(function (module) {

    'use strict';

    module.controller('IndexCtrl', [
        '$uibModal',
        '$location',
        controller
    ]);

    function controller($uibModal, $location) {
        var index = this;

        index.location = $location;

        index.newPhraseDialog = function () {
            var parentElem = angular.element(document.body);
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'add-phrase.modal.html',
                controller: 'AddPhraseModalCtrl',
                controllerAs: 'ctrl',
                size: 'md',
                appendTo: parentElem
            });

        };

    };

})(angular.module('app.controller'));

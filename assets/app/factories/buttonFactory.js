(function (module) {
  'use strict'
  module.factory('buttonFactory', factory);

  function factory() {

    function ButtonFactory() {
      this.data = {
        'row1': [
          { letter: 'A', disabled: false },
          { letter: 'B', disabled: false },
          { letter: 'C', disabled: false },
          { letter: 'D', disabled: false },
          { letter: 'E', disabled: false },
          { letter: 'F', disabled: false },
          { letter: 'G', disabled: false },
          { letter: 'H', disabled: false },
          { letter: 'I', disabled: false }
        ],
        'row2': [
          { letter: 'J', disabled: false },
          { letter: 'K', disabled: false },
          { letter: 'L', disabled: false },
          { letter: 'M', disabled: false },
          { letter: 'N', disabled: false },
          { letter: 'Ñ', disabled: false },
          { letter: 'O', disabled: false },
          { letter: 'P', disabled: false },
          { letter: 'Q', disabled: false },
          { letter: 'R', disabled: false }
        ],
        'row3': [
          { letter: 'S', disabled: false },
          { letter: 'T', disabled: false },
          { letter: 'U', disabled: false },
          { letter: 'V', disabled: false },
          { letter: 'W', disabled: false },
          { letter: 'X', disabled: false },
          { letter: 'Y', disabled: false },
          { letter: 'Z', disabled: false }
        ]
      }
    }

    ButtonFactory.prototype.reset = function () {
      var data = this.data;
      for (var key in data) {
        var buttons = data[key];
        for (var i = buttons.length; i--;) {
          buttons[i].disabled = false;
        }
      }
    }

    ButtonFactory.prototype.disableAll = function () {
      var data = this.data;
      for (var key in data) {
        var buttons = data[key];
        for (var i = buttons.length; i--;) {
          buttons[i].disabled = true;
        }
      }
    }

    return new ButtonFactory;
  }
})(angular.module('app.factory'));
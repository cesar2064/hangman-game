(function (module) {
  'use strict'
  /**
   * @ngdoc service
   * @name app.factory:playerFactory
   * @description Handles and stores the player data.
   */
  module.factory('playerFactory', factory);

  function factory() {

    var Player = function (name) {
      this.name = name;
      this.won = 0;
      this.lost = 0;
    };

    var PlayerFactory = function () {
      this.player = null;
      this.ready = false;
    };

    PlayerFactory.prototype.generate = function () {
      this.data = new Player('')
    };

    return new PlayerFactory();
  }
})(angular.module('app.factory'));
(function (module) {
  'use strict'
  /**
   * @ngdoc service
   * @name app.service:playerFactory
   * @description Handles the playerdata from the server.
   */
  module.service('playerService', [
    'CONSTANTS',
    '$interpolate',
    '$http',
    service
  ]);

  function service(CONSTANTS, $interpolate, $http) {

    this.save = function (player) {

      var path = CONSTANTS.PLAYER.PATHS.SAVE
      return $http({
        'url': path.URL,
        'method': path.METHOD,
        'data': player
      });
    };

    this.getAll = function () {
      var path = CONSTANTS.PLAYER.PATHS.GETALL
      return $http({
        'url': path.URL,
        'method': path.METHOD
      });
    };

  }
  
})(angular.module('app.service'));
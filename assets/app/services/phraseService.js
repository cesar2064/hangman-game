(function (module) {

  'use strict'
  module.service('phraseService', [
    'CONSTANTS',
    '$http',
    service
  ]);

  function service(CONSTANTS, $http) {

    var paths = CONSTANTS.PHRASE.PATHS;

    this.getAll = function () {
      var path = paths.GETALL
      return $http({
        'url': path.URL,
        'method': path.METHOD
      }).then(function (response) {
        return response.data;
      });
    };

    this.getRandomIdNotIn = function (ids) {
      var path = paths.RANDOM
      return $http({
        'url': path.URL,
        'params': { idNotIn: JSON.stringify(ids) },
        'method': path.METHOD
      }).then(function (response) {
        return response.data;
      });
    };

    this.getRandom = function () {
      var path = paths.RANDOM
      return $http({
        'url': path.URL,
        'method': path.METHOD
      }).then(function (response) {
        return response.data;
      });
    };

    this.save = function (phrase) {
      var path = paths.SAVE
      return $http({
        'url': path.URL,
        'data':phrase,
        'method': path.METHOD
      }).then(function (response) {
        return response.data;
      });
    }

  }
})(angular.module('app.service'));
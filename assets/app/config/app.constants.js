(function (app) {
  'use strict'

  var CONSTANTS = {
    SERVER: {
      IP: 'http://localhost',
      PORT: 3000,
      APIPATH: '/api/v1',
      DEFAULTCONTENTTYPE: 'application/json',
      get API() {
        return this.IP + ':' + this.PORT + this.APIPATH
      }
    },
    get PLAYER() {
      var api = this.SERVER.API + '/player';
      return {
        PATHS: {
          SAVE: {
            URL: api,
            METHOD: 'PUT'
          },
          GETALL: {
            URL: api,
            METHOD: 'GET'
          }
        }
      }
    },
    MOVE: {
      MAX: '5'
    },
    ROUNDS: {
      MAX: 3
    },
    get PHRASE() {
      var api = this.SERVER.API + '/phrase';
      return {
        PATHS: {
          GETALL: {
            URL: api,
            METHOD: 'GET'
          },
          RANDOM: {
            URL: api + '/random',
            METHOD: 'GET'
          },
          SAVE: {
            URL: api,
            METHOD: 'POST'
          }
        }
      }
    }
  };


  app.constant('CONSTANTS', CONSTANTS);



})(angular.module('app'));

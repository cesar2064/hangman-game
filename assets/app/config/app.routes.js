(function(app) {
    'use strict'
    /**
     * @ngdoc overview
     * @name app.config:routes
     * @description
     *   The application routes.
     */
    app.config([
        '$routeProvider',
        routes
    ]);

    function routes($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'enter-player.view.html',
                controller: 'EnterPlayerCtrl as ctrl',
                resolve: {
                    check: [
                        'playerFactory',
                        function(playerFactory) {
                            //generating a new player
                            playerFactory.generate();
                        }
                    ]
                }
            })
            .when('/rounds', {
                templateUrl: 'round.view.html',
                controller: 'RoundCtrl as ctrl',
                resolve: {
                    'firstPhrase': [
                        '$location',
                        'playerFactory',
                        'phraseService',
                        'buttonFactory',
                        function($location, playerFactory, phraseService,buttonFactory) {                            
                            if (!playerFactory.ready) {
                                $location.path('/');
                            } else {
                                buttonFactory.reset();
                                return phraseService.getRandom()
                                    .catch(function(response) {
                                        console.error('Fetching round data error ', response.status, response.data)
                                    });
                            }
                        }
                    ]
                }
            })
            .when('/conqueror', {
                templateUrl: 'conqueror.view.html',
                controller: 'ConquerorCtrl as ctrl',
                resolve: {
                    check: [
                        '$location',
                        'playerFactory',
                        'playerService',
                        function($location, playerFactory, playerService) {
                            var player = playerFactory.data;
                            if (
                                player === undefined ||
                                (player.won === 0 && player.lost === 0)
                            ) {
                                $location.path('/');
                            } else {
                                return playerService.save(player)
                                    .catch(function(response) {
                                        console.error('Can not save player results', response.status, response.data)
                                    });
                            }
                        }
                    ]
                }
            })
            .otherwise({
                redirectTo: '/'
            });
    };

})(angular.module('app'));

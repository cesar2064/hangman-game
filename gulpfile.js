const bowerFolder = 'bower_components'
const src = 'assets'
const dest = './.tmp/public'
const libs = dest + '/lib'
const e2eFolder = 'test-front/e2e'
const gulp = require('gulp')
const del = require('del')
const htmlmin = require('gulp-htmlmin')
const gulpProtractorAngular = require('gulp-angular-protractor')
const angularInlineTemplates = require('gulp-angular-inline-template')
const splice = require('gulp-splice')
const rename = require('gulp-rename')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const addsrc = require('gulp-add-src')
const strip = require('gulp-strip-comments')
const cleanCSS = require('gulp-clean-css')
const jsLibraries = `./${src}/gulp/bowerJs.json`
const cssLibraries = `./${src}/gulp/bowerCss.json`
const jsResources = `./${src}/gulp/js.json`
const cssResources = `./${src}/gulp/css.json`
const htmlReplace = require('gulp-html-replace')
const fs = require('fs')
const watch = require('gulp-watch')
const bowerJsLibraries = require(jsLibraries).map((item) => {
  return `${bowerFolder}/${item}`
})

const bowerCssLibraries = require(cssLibraries).map((item) => {
  return `${bowerFolder}/${item}`
})

const appJsResources = require(jsResources).map((item) => {
  return `${src}/${item}`
})

const appCssResources = require(cssResources).map((item) => {
  return `${src}/${item}`
})

function readLiveJson(relativePath) {
  return JSON.parse(fs.readFileSync(`${__dirname}/${relativePath}`))
}

// cleans the tmp directory
gulp.task('clean', () => {
  return del.sync([`${dest}/**/*`])
})

// Watches the assets and copy
gulp.task('copyAndWatchAssets', () => {
  let files = [
    `${src}/**/*`,
    `!${src}/index.html`,
    `!${src}/gulp/**`
  ]
  return gulp.src(files, {
      base: src
    })
    .pipe(watch(files, {
      base: src
    }))
    .pipe(gulp.dest(dest))
})

// Watches the index html and compile
gulp.task('watchIndexHtml', () => {
  return gulp.watch([`${src}/index.html`], ['compileHtmlDev'])
})

// Watches the gulp sources, compile index html and copies the bower libraries
gulp.task('watchGulpBowerSources', () => {
  return gulp.watch([`${src}/gulp/**/bower*.json`], ['copyBowerLibraries', 'compileHtmlDev'])
})

// Watches the app sources, compile index html and copies de app libraries
gulp.task('watchGulpAppSources', () => {
  return gulp.watch([`${src}/gulp/**/*.json`, `!${src}/gulp/**/bower*.json`], ['compileHtmlDev'])
})

// copies the bower min libraries
gulp.task('copyBowerLibraries', () => {
  return gulp.src(bowerJsLibraries.concat(bowerCssLibraries), {
      base: 'bower_components'
    })
    .pipe(gulp.dest(`${dest}/libs`))
})

// compiles the index html for dev mode
gulp.task('compileHtmlDev', () => {
  return gulp.src([`${src}/index.html`])
    .pipe(htmlReplace({
      js: readLiveJson(jsLibraries).map((item) => {
        return `/libs/${item}`
      }).concat(
        readLiveJson(jsResources).map((item) => {
          return `/${item}`
        })),
      css: readLiveJson(cssLibraries).map((item) => {
        return `/libs/${item}`
      }).concat(
        readLiveJson(cssResources).map((item) => {
          return `/${item}`
        })
      )
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest(dest))
})

// Compile the index html and angular templates into one file for production mode
gulp.task('compileHtml', () => {
  return gulp.src([`${src}/*.view.html`,`${src}/*.modal.html`])
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(angularInlineTemplates())
    .pipe(splice({
      key: '<!--compiledDirectives-->',
      outer: `${src}/index.html`
    }))
    .pipe(htmlReplace({
      js: 'script.js',
      css: 'styles.css'
    }))
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest(dest))
})

//Compiles javascript files for production mode
gulp.task('compileJS', () => {
  return gulp.src(appJsResources)
    .pipe(uglify())
    .pipe(addsrc.prepend(bowerJsLibraries))
    .pipe(concat('script.js'))
    .pipe(strip())
    .pipe(gulp.dest(dest))
})
//Compiles css files for production mode
gulp.task('compileCss', () => {
  return gulp.src(appCssResources)
    .pipe(cleanCSS())
    .pipe(addsrc.prepend(bowerCssLibraries))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(dest))
})
//Starts the trails application
gulp.task('start-trails', () => {
  require('./server.js')
})

//Tasks for development mode
gulp.task('development', [
  'clean',
  'copyBowerLibraries',
  'copyAndWatchAssets',
  'compileHtmlDev',
  'watchIndexHtml',
  'watchGulpBowerSources',
  'watchGulpAppSources',
  'start-trails'
])
//Tasks for production mode
gulp.task('production', ['clean', 'compileJS', 'compileHtml', 'compileCss','start-trails'])
'use strict'

const Model = require('trails-model')

/**
 * @module Phrase
 * @description Handles the game phrases
 */
module.exports = class Phrase extends Model {

  static config(app, Sequelize) {
    return {
      options: {
        hooks: {
          beforeCreate: function (phrase) {
            phrase.text = phrase.text.trim();
          },
          beforeUpdate: function (phrase) {
            phrase.text = phrase.text.trim();
          }
        }
      }
    };
  }

  static schema(app, Sequelize) {
    return {
      text: {
        type: Sequelize.STRING,
        validate: {
          is: /^[a-zA-Z\s]+$/i,
          min: 3,
        },
        allowNull: false,
        unique: true
      }
    }
  }
}

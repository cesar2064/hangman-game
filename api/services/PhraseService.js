'use strict'

const Service = require('trails-service')

/**
 * @module PhraseService
 * @description Handles phrase bussiness logic
 */
module.exports = class PhraseService extends Service {

    getAll() {
        return this.app.orm.Phrase.findAll().then((results) => {
            if (results) {
                return results;
            }
            return [];
        });
    }

    save(phrase) {
        return this.app.orm.Phrase.create(phrase);
    }

    randomPhraseIdNotIn(ids) {
        var phraseModel = this.app.orm.Phrase;
        let query = {
            get where() {
                if (ids.length === 0) {
                    return {};
                }
                return {
                    id: {
                        $and: {
                            $notIn: ids
                        }
                    }
                }
            },
            order: [
                phraseModel.sequelize.fn('RANDOM')
            ]
        };
        return phraseModel.find(query)
    }
}


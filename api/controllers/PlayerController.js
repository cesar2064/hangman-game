'use strict'

const Controller = require('trails-controller')

/**
 * @module PlayerController
 * @description Generated Trails.js Controller.
 */
module.exports = class PlayerController extends Controller {
  /**
   * @function getAll
   * @description Returns all players.
   * 
   */
  getAll(req, res) {
    return this.app.services.PlayerService.findAll().then((players) => {
      return res.status(200).json(players)
    });
  }

  /**
   * @function getAll
   * @description Returns a players by name.
   * 
   */
  getByName(req, res) {
    var name = req.params.name 
    return this.app.services.PlayerService.findByName(name).then((player) => {
      return res.status(200).json(player)
    });
  }

  /**
   * @function save
   * @description saves a player.
   */
  save(req, res) {
    let user = req.body;    
    return this.app.services.PlayerService.save(user).then((savedPlayer) => {
      return res.status(200).json(savedPlayer);
    });
  }
}


'use strict'

const Controller = require('trails-controller')

/**
 * @module PhraseController
 * @description Generated Trails.js Controller.
 */
module.exports = class PhraseController extends Controller {

    findAll(req, res) {
        return this.app.services.PhraseService.getAll().then((results) => {
            return res.status(200).json(results);
        });
    }

    save(req, res) {        
        return this.app.services.PhraseService.save(req.body)
            .then((results) => {
                return res.status(200).json(results);
            })
            .catch((error)=>{
                console.log(error)
                return res.status(417).json(error);
            })
    }

    randomPhraseNotIn(req, res) {
        var ids = req.query.idNotIn;
        return this.app.services.PhraseService.randomPhraseIdNotIn(
            ids ? JSON.parse(ids) : []
        )
            .then((result) => {
                return res.status(200).json(result);
            });
    }

}


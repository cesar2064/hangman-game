const TrailsApp = require('trails');
const SpecReporter = require('jasmine-spec-reporter');

exports.config = {
  specs: [
    'specs/enter-player.spec.js',
    'specs/round-lost.spec.js',
    'specs/conqueror-lost.spec.js',
    'specs/round-won.spec.js'
  ],
  mocks: {
    default: [], // default value: []
    dir: 'http-mocks' // default value: 'mocks'
  },
  rootElement: 'body',
  onPrepare: function () {


    // add jasmine spec reporter      
    jasmine.getEnv().addReporter(new SpecReporter({
      displayStacktrace: 'all'
    }));
    require('protractor-http-mock').config = {
      rootDirectory: __dirname, // default value: process.cwd()
      protractorConfig: 'protractor.conf.js' // default value: 'protractor-conf.js'
    };
    //starting the trails application
    global.app = new TrailsApp(require('../../'))
    return global.app.start().catch(global.app.stop);
  },
  onCleanUp: function (exitCode) {
    return global.app.stop()
  }
};
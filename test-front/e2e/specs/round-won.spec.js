var mock = require('protractor-http-mock');

mock(['phrase', 'players'])

var urlChanged = function (url) {
  return function () {
    return browser.getCurrentUrl().then(function (actualUrl) {
      return actualUrl.indexOf(url) >= 0;
    });
  };
};

var roundNumber = 'h3 strong';

function write(letters) {
  letters.forEach(function (letter) {
    element(by.buttonText(letter)).click();
  });
}

describe('round win', function () {

  it('should be on the rounds view ', function () {
    element(by.model('ctrl.player.name')).sendKeys('Player 1');
    element(by.buttonText('Start!')).click();
    browser.wait(urlChanged("/#/rounds"), 2000);
  });

  it('Should be the first round', function () {
    expect(element(by.css(roundNumber)).getText()).toContain('Round 1');
  });

  it('Should show the next button after the player guessed the word', function () {
    write(['M', 'O', 'C', 'K', 'P', 'H', 'R', 'A', 'S', 'E']);
    browser.waitForAngular();
    expect(element(by.css('button[class="btn btn-primary"]')).isPresent()).toBeTruthy();
  });

  it('Should be the second round', function () {
    element(by.css('button[class="btn btn-primary"]')).click();
    browser.waitForAngular();
    expect(element(by.css(roundNumber)).getText()).toContain('Round 2');
  });

  it('Should show the next button after the player guessed the word', function () {
    write(['M', 'O', 'C', 'K', 'P', 'H', 'R', 'A', 'S', 'E']);
    browser.waitForAngular();
    expect(element(by.css('button[class="btn btn-primary"]')).isPresent()).toBeTruthy();
  });

  it('Should be the third round', function () {
    element(by.css('button[class="btn btn-primary"]')).click();
    browser.waitForAngular();
    expect(element(by.css(roundNumber)).getText()).toContain('Round 3');
  });


  describe('Player won all rounds', function () {
    it('Should be on the conqueror page', function () {
      write(['M', 'O', 'C', 'K', 'P', 'H', 'R', 'A', 'S', 'E']);
      browser.waitForAngular();
      browser.wait(urlChanged("/#/conqueror"), 2000);
    })
  });

})
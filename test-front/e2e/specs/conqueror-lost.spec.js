describe('conqueror lost', function() {

    var urlChanged = function(url) {
        return function() {
            return browser.getCurrentUrl().then(function(actualUrl) {
                return actualUrl.indexOf(url) >= 0;
            });
        };
    };

    it('should display that the player lost',function(){
      expect(element(by.css('div[class="form-group alert alert-danger"]')).isPresent()).toBeTruthy();
    });

    it('Clicking on play again button should change the page to the main url',function(){
      element(by.buttonText('Play Again')).click();
      browser.wait(urlChanged("/#/"), 2000);
    });
})
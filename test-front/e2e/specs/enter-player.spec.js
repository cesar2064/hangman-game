var mock = require('protractor-http-mock');
mock(['players', 'phrase'])

describe('enter-players view', function () {

  afterEach(function () {
    mock.teardown();
  });


  var playerInput = element(by.model('ctrl.player.name'))
  var errorSelector = '.has-error';
  var newPhraseModelInput = 'input[ng-model="ctrl.phrase.text"]';
  var addNewPhraseLink = 'button[ng-click="index.newPhraseDialog()"]';

  browser.get('http://localhost:3000');

  var urlChanged = function (url) {
    return function () {
      return browser.getCurrentUrl().then(function (actualUrl) {
        return actualUrl.indexOf(url) >= 0;
      });
    };
  };

  describe('Click on the start button', function () {
    describe('with out values', function () {
      it('Should be stay in the main page', function () {
        element(by.buttonText('Start!')).click();
        browser.wait(urlChanged("/#/"), 2000);
      });
      it('Should showing the player input error', function () {
        expect(element(by.css(errorSelector)).isPresent()).toBeTruthy();
      })
    });

  });
  describe('Click on the add a new phrase button', function () {
    it('Should open the modal', function () {
      element(by.css(addNewPhraseLink)).click();
      expect(element(by.css(newPhraseModelInput)).isPresent()).toBeTruthy();
    });
  });

  describe('New phrase modal input', function () {
    it('Should close the modal if the user clicks on the close button', function () {
      element(by.css('button[class="btn btn-warning"]')).click();
      expect(element(by.css(newPhraseModelInput)).isPresent()).toBeFalsy();
    })
  });

  describe('Writing a player name', function () {

    it('Should not showing errors when writing a correct player name', function () {
      playerInput.sendKeys('Player 1');
      expect(element(by.css(errorSelector)).isPresent()).toBeFalsy();
    });

    it('Should showing an error when writing an invalid player name', function () {
      playerInput.click().clear().then(function () {
        expect(element(by.css(errorSelector)).isPresent()).toBeTruthy();
      });
    });

  });

  describe('Click on the start button with valid values', function () {
    it('Should go to the rounds page', function () {
      playerInput.click().clear().then(function () {
        playerInput.sendKeys('Player 1');        
        element(by.buttonText('Start!')).click();
        browser.wait(urlChanged("/#/rounds"), 2000);
      });
    });

  });

});
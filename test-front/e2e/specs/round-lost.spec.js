var mock = require('protractor-http-mock');

mock(['phrase','players'])

describe('rounds lost', function() {
    
    var urlChanged = function(url) {
        return function() {
            return browser.getCurrentUrl().then(function(actualUrl) {
                return actualUrl.indexOf(url) >= 0;
            });
        };
    };

    describe('Player loses a round', function() {        

        it('Should move the the conqueror when the player lost after five wrong letters', function() {
            var buttonx= element(by.buttonText('X'));
            buttonx.click();
            buttonx.click();
            buttonx.click();
            buttonx.click();
            buttonx.click();
        });
    });

    describe('Move to the conqueror page', function() {

        it('Should be on the conqueror page', function() {
            browser.wait(urlChanged("/#/conqueror"), 2000);
        });

    });

})
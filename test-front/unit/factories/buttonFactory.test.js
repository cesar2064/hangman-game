describe('buttonFactory', function () {

  var _buttonFactory;

  function checkButtons(cbChecker) {
    var buttons = _buttonFactory.data;
    for (var key in buttons) {
      buttons[key].forEach(cbChecker);
    }
  }

  beforeEach(inject(function (buttonFactory) {
    _buttonFactory = buttonFactory;
  }));

  describe('disableAll', function () {

    it('Should disable all buttons', function () {
      _buttonFactory.disableAll();
      var allAreDisabled = true;

      checkButtons(function (button) {
        if (!button.disabled) {
          allAreDisabled = false;
        }
      })

      expect(allAreDisabled).toEqual(true);
    });

  });

  describe('reset', function () {

    it('Should enable all buttons', function () {
      _buttonFactory.reset();

      var allAreEnabled = true;

      checkButtons(function (button) {
        if (button.disabled) {
          allAreEnabled = false;
        }
      })

      expect(allAreEnabled).toEqual(true);
    });

  });
});
describe('RoundCtrl', function () {
  var controller;
  var _constants;
  var location;

  function buttonInput(letter) {
    var button = { letter: letter, disabled: false };
    controller.playerInput(button);
    return button;
  };

  function pressAndMatchAll() {
    controller.splittedPhrase.forEach(function (letter) {
      if (letter.value != ' ') {
        buttonInput(letter.value);
      }
    });
  };

  beforeEach(inject(function ($controller, CONSTANTS, $location) {
    controller = $controller('RoundCtrl', {
      firstPhrase: { text: 'your mom' }
    });
    controller.player = {
      name: 'cesar'
    }
    _constants = CONSTANTS;

    spyOn($location, "path").and.callFake(function () {
      return;
    });
    location = $location;
  }));

  describe('splitPhrase', function () {
    it('Should return the expected array with a phrase with no spaces', function () {
      expect(controller.splitPhrase('gato')).toEqual([
        { value: 'G', show: false },
        { value: 'A', show: false },
        { value: 'T', show: false },
        { value: 'O', show: false }
      ]);
    });
    it('Should return the expected array with a phrase with spaces', function () {
      expect(controller.splitPhrase('gato malo')).toEqual([
        { value: 'G', show: false },
        { value: 'A', show: false },
        { value: 'T', show: false },
        { value: 'O', show: false },
        { value: ' ', show: true },
        { value: 'M', show: false },
        { value: 'A', show: false },
        { value: 'L', show: false },
        { value: 'O', show: false }
      ]);
    });
  });

  describe('matchLetter', function () {
    it('Should return false if a letter doesn\'t match', function () {
      expect(controller.matchLetter('x')).toEqual(false);
    });
    it('Should return true if a letter matches', function () {
      expect(controller.matchLetter('o')).toEqual(true);
    });
  });

  describe('playerInput', function () {
    it('Should disable the letter button if a letter matches', function () {
      var button = buttonInput('y');
      expect(button.disabled).toEqual(true);
    });
    it('Should substract one to the moves left if the letter button doesn\'t match', function () {
      buttonInput('x');
      expect(_constants.MOVE.MAX - controller.movesLeft).toEqual(1);
    });
    it('Should call the location path when there are not moves left', function () {
      controller.movesLeft = 1;
      buttonInput('x');
      expect(location.path.calls.count()).toEqual(1);
    });
    it('Should display the next button when the user matches all letters', function () {
      pressAndMatchAll();
      expect(controller.nextButton).toEqual(true);
    });
    it('Should call the location path when the user matches all letters and all round are complete', function () {
      controller.roundIndex = _constants.ROUNDS.MAX;
      pressAndMatchAll();
      expect(location.path.calls.count()).toEqual(1);
    });    
  })
})
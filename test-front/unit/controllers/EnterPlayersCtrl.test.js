describe('EnterPlayersCtrl', function () {
	var controller;
	var _playerService;
	var scope;
	var location;
	var compile;
	var playerFormTemplate = angular.element(
		'<form novalidate  name="ctrl.playerForm" class="form-horizontal enterPlayers">' +
		'<input ng-model="ctrl.player.name" name="playerName" type="text" class="form-control" required=""/>' +
		'</form>'
	);

	function recompilePlayerForm() {

		scope.ctrl = controller;

		compile(playerFormTemplate)(scope);

		scope.$digest();

		controller.playerForm = scope.ctrl.playerForm;

		form = controller.playerForm;
	}


	beforeEach(inject(function ($controller, playerService, $q, $rootScope, $compile, $location) {

		_playerService = playerService;

		spyOn(playerService, "getAll").and.callFake(function () {
			var deferred = $q.defer();
			deferred.resolve([]);
			return deferred.promise;
		});

		spyOn($location, "path").and.callFake(function () {
			return;
		});

		location = $location;

		controller = $controller('EnterPlayerCtrl');

		scope = $rootScope.$new();

		compile = $compile;
	}));

	describe('validatePlayerName function', function () {

		describe('On empty name and dirty', function () {
			it('Should return true', function () {

				controller.player = {
					name: ''
				}

				recompilePlayerForm();

				form.playerName.$dirty = true;

				expect(controller.validatePlayerName()).toEqual(true);
			});

		});

		describe('On valid name and dirty', function () {
			it('Should return false', function () {

				controller.player = {
					name: 'cesar'
				}
				recompilePlayerForm();

				form.playerName.$dirty = true;

				expect(controller.validatePlayerName()).toEqual(false);
			});

		});

	});

	describe('start function', function () {
		describe('On inValid name', function () {
			it('Should should put the player dirty state in true', function () {
				controller.player = {
					name: ''
				}
				recompilePlayerForm();
				controller.start();
				expect(form.playerName.$dirty).toEqual(true);
			});
		});

		describe('On Valid name', function () {
			it('Should call the location path function', function () {
				controller.player = {
					name: 'cesar'
				}
				recompilePlayerForm();
				controller.start();
				expect(location.path.calls.count()).toEqual(1);
			});
		});
	});

});